<%-- 
    Document   : delete
    Created on : Feb 10, 2017, 2:40:49 PM
    Author     : firmanmac
--%>

<%@page import="com.dbconnectionutil.org.DbConnection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
        int row=0;
        String id = request.getParameter("id");
        int final_id = Integer.parseInt(id);
        Connection conn = null;
        PreparedStatement ps = null;
        try{
            conn = DbConnection.getConnection();
            ps = conn.prepareStatement("delete from tb_visit where id=?");
            ps.setInt(1, final_id);
            row = ps.executeUpdate();            
            if(row>0){
                response.sendRedirect("index.jsp");
                //out.println("Records deleted "+row);
            }else{
                out.println("Error in query!");
            }
        }catch(Exception ex){
            out.println(ex.getMessage());
        }
        
        finally{
            ps.close();
            conn.close();
        }
        %>
    </body>
</html>
