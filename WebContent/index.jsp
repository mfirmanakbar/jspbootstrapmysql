<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.dbconnectionutil.org.DbConnection"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	   <title>Hello</title>
	   <meta charset="utf-8">
	   <meta name="viewport" content="width=device-width, initial-scale=1">        
	   <meta http-equiv="X-UA-Compatible" content="IE=edge">
	   <meta name="viewport" content="width=device-width, initial-scale=1">
	   <meta name="description" content="">
	   <meta name="author" content="">
	   <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	</head>
<body>
    <div class="container">
       <h1 align="center">CRUD JSP MYSQL</h1>
       <hr/>
       <div class="row">
           <div class="col-lg-4">
               <form role="form" action="addRecord.jsp" method="post">
                   <div class="form-group">
                       <label for="firstname">First Name</label>
                       <input name="txtfname" type="text" class="form-control" id="firstname" placeholder="First Name" required="required">
                   </div>
                   <div class="form-group">
                       <label for="lastname">Last Name</label>
                       <input name="txtlname" type="text" class="form-control" id="lastname" placeholder="Last Name" required="required">
                   </div>
                   <div class="form-group">
                       <label for="email">Email</label>
                       <input name="txtemail" type="email" class="form-control" id="email" placeholder="Email" required="required">
                   </div>
                   <button type="submit" class="btn btn-success btn-block">Submit</button>
               </form>
           </div>
           <div class="col-lg-8">
               <table class="table table-hover">
                   <thead>
                       <tr>
                           <td><b>No</b></td>
                           <td><b>First Name</b></td>
                           <td><b>Last Name</b></td>
                           <td><b>Email</b></td>
                           <td><b>Actions</b></td>
                       </tr>
                   </thead>
                   <tbody>
                       <%
                       Connection conn = null;
                       PreparedStatement ps = null;
                       ResultSet rs = null;
                       String query = "SELECT * FROM tb_visit";

                       //conn = DbConnection.getConnection();
                       //out.println(DbConnection.dbresult);

                       try{
                           conn = DbConnection.getConnection();
                           ps = conn.prepareStatement(query);
                           rs = ps.executeQuery();
                           int nomor=1;
                           while(rs.next()){
                       %>
                       <tr>
                           <td><% out.print(nomor); %></td>
                           <td><%=rs.getString("fname") %></td>
                           <td><%=rs.getString("lname") %></td>
                           <td><%=rs.getString("email") %>.com</td>
                           <td>
                               <a href="editform.jsp?id=<%=rs.getInt("id")%>" class="btn btn-primary btn-xs" role="button">Update</a>
                               <a href="delete.jsp?id=<%=rs.getInt("id")%>" class="btn btn-danger btn-xs" role="button">Delete</a>
                           </td>
                       </tr>
                       <%
                           nomor++;
                           }
                       }catch(Exception ex){
                           out.println(ex.getMessage());
                       }
                       %>
                   </tbody>
               </table>
           </div>
       </div>
   </div>
   
   <script src="assets/js/jquery.min.js"></script>
   <script src="assets/js/bootstrap.min.js"></script>
</body>
</html>