<%-- 
    Document   : update
    Created on : Feb 10, 2017, 2:27:48 PM
    Author     : firmanmac
--%>

<%@page import="com.dbconnectionutil.org.DbConnection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
        String id= request.getParameter("txteid");
        int final_id = Integer.parseInt(id);
        String fname = request.getParameter("txtefname");
        String lname = request.getParameter("txtelname");
        String email = request.getParameter("txteemail");
        Connection conn = null;
        PreparedStatement ps = null;
        
        try{
            int row = 0;
            conn = DbConnection.getConnection();
            ps = conn.prepareStatement("update tb_visit set fname=?, lname=?, email=? where id=?");
            ps.setString(1, fname);
            ps.setString(2, lname);
            ps.setString(3, email);
            ps.setInt(4, final_id);
            
            row = ps.executeUpdate();            
            if(row>0){
                response.sendRedirect("index.jsp");
                //out.println("Records update "+row);
            }else{
                out.println("Error in query!");
            }
        }catch(Exception ex){
            out.println(ex.getMessage());
        }
        %>
    </body>
</html>
