<%-- 
    Document   : editform
    Created on : Feb 10, 2017, 2:00:01 PM
    Author     : firmanmac
--%>

<%@page import="com.dbconnectionutil.org.DbConnection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>EDIT</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="shortcut icon" type="image/ico" href="assets/img/logo1.png">-->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="assets/css/navbar-fixed-top.css" rel="stylesheet">-->
    </head>
    <body>
        <div class="container">
            <h1 align="center">Update</h1>
            <hr/>
            <div class="row">
                <div class="col-lg-4">                    
                </div>
                <div class="col-lg-4">
                    <%
                    String id = request.getParameter("id");
                    int final_id = Integer.parseInt(id);
                    Connection conn = null;
                    PreparedStatement ps = null;
                    ResultSet rs = null;
                    try{
                        conn = DbConnection.getConnection();
                        String query = "select * from tb_visit where id="+final_id;
                        ps = conn.prepareStatement(query);
                        rs = ps.executeQuery();
                        while(rs.next()){
                    %>
                    <form role="form" action="update.jsp" method="post">
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input value="<%=rs.getInt("id")%>" name="txteid" type="hidden" required="required">
                            <input value="<%=rs.getString("fname")%>" name="txtefname" type="text" class="form-control" id="firstname" placeholder="First Name" required="required">
                        </div>
                        <div class="form-group">
                            <label for="lastname">Last Name</label>
                            <input value="<%=rs.getString("lname")%>" name="txtelname" type="text" class="form-control" id="lastname" placeholder="Last Name" required="required">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input value="<%=rs.getString("email")%>" name="txteemail" type="email" class="form-control" id="email" placeholder="Email" required="required">
                        </div>
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </form>
                    <%
                        }
                    }catch(Exception ex){
                        out.println(ex.getMessage());
                    }
                    %>
                </div>
                <div class="col-lg-4">                    
                </div>
            </div>
        </div>
    </body>
</html>
