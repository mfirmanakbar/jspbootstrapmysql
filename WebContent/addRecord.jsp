<%-- 
    Document   : addRecord
    Created on : Feb 10, 2017, 10:26:29 AM
    Author     : firmanmac
--%>

<%@page import="com.dbconnectionutil.org.DbConnection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert Page</title>
    </head>
    <body>
        <%
            String fname = request.getParameter("txtfname");
            String lname = request.getParameter("txtlname");
            String email = request.getParameter("txtemail");
            
            Connection conn = null;
            PreparedStatement ps = null;
            
            try{
                int row = 0;
                conn = DbConnection.getConnection();
                String query = "insert into tb_visit (fname,lname,email) values (?,?,?)"; 
                ps = conn.prepareStatement(query);
                ps.setString(1, fname);
                ps.setString(2, lname);
                ps.setString(3, email);
                row = ps.executeUpdate();
                if(row>0){
                    response.sendRedirect("index.jsp");
                    //out.println("Record Add into table!");
                }else{
                    out.println("Error In Query!");
                }
            }catch(Exception ex){
                out.println(ex.getMessage());
            }
        %>
    </body>
</html>
