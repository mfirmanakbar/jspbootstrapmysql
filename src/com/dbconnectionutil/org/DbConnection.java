package com.dbconnectionutil.org;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {

	public static String dbresult = null;
    static String HOST="jdbc:mysql://localhost:3307/tem_all";
    static String USER="root";
    static String PASSWORD="";
    static Connection conn=null;
    
    public static Connection getConnection(){
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(HOST, USER, PASSWORD);
            dbresult = "Connected Successfully In Database!";
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return conn;
        
    }
	
}
